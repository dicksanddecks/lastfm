import numpy as np
import subprocess
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import tree
from sklearn import metrics
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import MultinomialNB

def main():
	# Load train e test
	
	train = pd.read_csv('training.csv', skipinitialspace=True)
	test = pd.read_csv('test.csv', skipinitialspace=True)
	
	print len(test)
	pred = test["Abbandona"].tolist()
	x = train.drop("nome",1)
	
	y = x["Abbandona"]
	
	x = x.drop("Abbandona",1)
	
	test = test.drop("nome",1)
	
	test = test.drop("Abbandona",1)
	
	decision_tree(x,y)
	
	k_nn(x,y)
	
	svm(x,y)
	
	bayes(x,y)

def svm(x, y):
	
	massimo = 0
	kernelmax = "a"
	cmax = 0
	gammamax = 0
	listkernel = ['rbf', 'sigmoid']
	clist = [1,10,100,1000]
	gammalist = [0, 1, 10, 100]

	for item3 in listkernel:
		
		for item2 in clist:
			
			for item in gammalist:
			

				clf = svm.SVC(kernel=item3, C=item2, gamma=item).fit(x,y)
							
				predicted = clf.predict(test)

				accuracy = accuracy_score(pred, predicted)
				print accuracy
				
				if accuracy > massimo:
					
					massimo = accuracy
					kernelmax=item3
					cmax=item2
					gammamax=item
	
	print "\n"
	print massimo
	print kernelmax
	print cmax
	print gammamax

def bayes(x, y):
	
	#model = GaussianNB()
	model = BernoulliNB()
	#model = MultinomialNB()

	# Train the model using the training sets 
	model.fit(x, y)

	#Predict Output 
	predicted = model.predict(test)
	
	accuracy = accuracy_score(pred, predicted)
	print accuracy
	
def k_nn(x, y):
	
	massimo = 0
	kmax = 0
	weightsmax = "a"
	algorithmmax = ""
	
	algorithmlist = ['auto', 'ball_tree', 'kd_tree', 'brute']
	weightlist = ['uniform', 'distance']
	
	for a in algorithmlist:
		for w in weightlist:
			for k in range(2, 100):
				
				neigh = KNeighborsClassifier(n_neighbors = k, weights = w, algorithm = a)
				
				neigh.fit(x, y) 
				
				predicted = neigh.predict(test)
				
				accuracy = accuracy_score(pred, predicted)
				print accuracy
				
				if accuracy > massimo:
							
					massimo = accuracy
					kmax = k
					weightsmax = w
					algorithmmax = a
					
	print "\n"
	print massimo
	print kmax
	print weightsmax
	print algorithmmax

def decision_tree(x, y):
	
	# Costruisco l'albero
	massimo = 0
	par1max = 0
	par2max = 0
	par3max = 0
	for par1 in range(1, 10):
		for par2 in range(1, 100):
			for par3 in range(1, 100):				
																											
				clf = tree.DecisionTreeClassifier(criterion="gini", splitter="random", max_depth = par1, min_samples_split=par2,min_samples_leaf=par3).fit(x,y)

				#Effettuo la previsione
				predicted = clf.predict(test)
				
				count = 0
				
				accuracy = accuracy_score(pred, predicted)
				print accuracy
				
				if accuracy > massimo and accuracy > 0.68:
					
					massimo = accuracy
					par1max = par1
					par2max = par2
					par3max = par3
					
					filename = "gini" + "_random_" + str(massimo) + "_" + str(par1max) + "_" + str(par2max) + "_" + str(par3max)
					#Visualizzo l'albero
					visualize_tree(clf,x.columns,"/tmp/", filename)


def visualize_tree(clf, feature_names, path, massimo):
	with open("/tmp/dt.dot", 'w') as f:
		tree.export_graphviz(clf, out_file=f,feature_names=feature_names)

	#command = ["dot", "-Tpng", "/tmp/dt.dot", "-o", path+"dt.png"]
	command = ["dot", "-Tpng", "/tmp/dt.dot", "-o", "dt_" + str(massimo) + ".png"]
	
	try:
		subprocess.check_call(command)
		subprocess.check_call(["rm", "/tmp/dt.dot"])
	except:
		exit("Subprocess command error")
		
		
if __name__ == "__main__":
	main()
