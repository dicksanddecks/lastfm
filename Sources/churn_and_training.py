import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import matplotlib.dates as md
import time
import dateutil
import operator
import datetime
from matplotlib.dates import YearLocator, MonthLocator, DateFormatter
from collections import Counter
from collections import defaultdict
from datetime import datetime
import collections
from operator import itemgetter
from statistics import mode
from statistics import StatisticsError

plt_data = []

def main():

	df = pd.read_csv("rock.csv", delimiter = ",", skipinitialspace = True)

    frequenza_utenti(df)

#metodo che estrae gli utenti con ascolti >= 85
def extract_freq_csv(data):
    
    data['count'] = data.groupby('user_id', as_index=False)['user_id'].transform(lambda s: s.count())
    data = data[data["count"] >= 85]
    data = data.drop("count", 1)
    
    return data

# modifica dell'attributo date:
# -rimozione degli zeri
# -rimozione delle ultime 3 cifre (gli zeri in eccesso)
# -converzione dell'attributo date da timestamp unix in data 
# -creazione della nuova colonna "date-ymd" che contiene anno, mese e giorno di quella riga
def preparation_date(df):
	
	df = df[df["date"] != 0]

	df["date"] = df["date"].map(lambda x: str(x)[:-3])
	df['date'] = pd.to_datetime(np.int32(df['date']),unit='s')
	
	df['date-ymd'] = df['date'].apply(lambda x: str(x.year) + "-" + str(x.month) + "-" + str(x.day))
	
	return df

#metodo che ci permette di plottare gli ascolti di ciascun utente sui giorni
def frequenza_utenti(df):
	
	df = extract_freq_csv(df)
	
	df = preparation_date(df)
	
	colum = ["nome", "rock", "pop", "punk", "metal", "folk", "electronic", "indie", "blues", "hip hop", "emo", "alternative", "altro", "abbandona"]
	df_completo = pd.read_csv("listenings_subst_genres_post_preparationDate.csv", delimiter = ",", skipinitialspace = True)
	
	print "utenti presenti"
	print len(df["user_id"].unique())
	
	countT = 0
	countF = 0
	count = 0
	
	num_generi = []
	
	for user in df["user_id"].unique():
		
		df2 = df[df["user_id"] == user]
		
		flag, generi, ascolti_rock = plot_allData_user(df2, user, df_completo)
		
		#print "Utente " + item + " abbandona?"
		#print flag
		#print "\n"
		
		if(flag == True):
			countT += 1
		if(flag == False):
			countF += 1
	
		count += 1
		print count
		
		saveoncsv(user, generi, colum, ascolti_rock, flag)
		
		if len(generi) > 0:
			for item in generi:
				num_generi.append(item)
	
	print "numero utenti che abbandonano"
	print countT
	print "altri"
	print countF
	
	# stampo su file il numero di ascolti di tutti gli utenti frequenti nel rock per ogni genere
	counter = collections.Counter(num_generi)
	print counter
	
	genere = counter.keys()
	values = counter.values()
	
	for genre, val in zip(genere, values):
		with open("listening_other_genres.txt", "a") as myfile:
			myfile.write(str(genre) + "-" + str(val))
			myfile.write("\n")

#plotto tutte le date di ciascun utente per vedere la frequenza dei loro ascolti (chiamata da frequenza_utenti) 
def plot_allData_user(data, user, df_completo):
	
	temp =  data["date"]

	counter = collections.Counter(temp)
	
	years = counter.keys()
	values = counter.values()
	
	x = pd.Series()
	
	for i in range(0, len(years)):
	   x = x.set_value(years[i], values[i])
	
	#converto le date in periodi di giorni
	x = x.resample("D", how = "count")
	
	dictionary = {}
	
	dictionary = x.to_dict()
	
	years = []
	values = []
	
	# ordino il dizionario secondo la data
	dictionary = collections.OrderedDict(sorted(dictionary.items()))
	
	for k,v in dictionary.items():
		years.append(k)
		values.append(v)
	
	flag = False
	flag = rule_70(years, values)
	if flag == False:
		flag = rule_40(years, values)
		if flag == False:
			flag = rule_90(years, values)
			if flag == False:
				flag = desc(values)
	
	generi = []

	generi, ascolti_rock = returnToDF(df_completo, years[0], years[-1], user)
	
	fig = plt.figure()
	ax = plt.subplot(111)
	plt.plot(years,values,'o-')
	plt.legend(loc=4)
	plt.ylabel('Listenings')
	plt.xlabel('Time')
	plt.grid()
	plt.show()
	
	return flag, generi, ascolti_rock

# questa funziona calcola per ogni utente, il numero di ascolti del genere rock
# e il numero di ascolti degli altri generi nel periodo del rock
def returnToDF(df, primo, ultimo, user):
	
	df = df[df["user_id"] == user]
	
	df2 = df[df["genre"] == "rock"]
	
	df = df[df["genre"] != "rock"]
	
	generi = []
	
	#for index, row in df.iterrows():
	
		#temp = row["date-ymd"].split("-")
		#temp = [int(i) for i in temp]
		#if (datetime(temp[0], temp[1], temp[2]) > datetime(primo.year, primo.month, primo.day) 
			#and datetime(temp[0], temp[1], temp[2]) < datetime(ultimo.year, ultimo.month, ultimo.day)):
				
				#generi.append(row["genre"])
	
	#return generi
	
	mask = (df['date-ymd'] > str(primo.year) + "-" + str(primo.month) + "-" + str(primo.day)) & (df['date-ymd'] < str(ultimo.year) + "-" + str(ultimo.month) + "-" + str(ultimo.day))
	
	df = df.loc[mask]
	
	generi = df["genre"].values.tolist()
	
	return generi, len(df2["genre"])

# salvo sul csv l'utente passato come parametro e il numero di ascolti per ogni genere e se abbandona
def saveoncsv(user, generi, colum, ascolti_rock, flag):
	
	counter = collections.Counter(generi)
	
	users = pd.DataFrame()
	dictionary = {}
	dictionary = dict(counter)
	
	pop = 0
	punk = 0
	metal = 0
	electronic = 0
	folk = 0
	indie = 0
	blues = 0
	hip_hop = 0
	emo = 0
	alternative = 0
	altro = 0
	
	for k,v in dictionary.items():
		if k == "pop":
			pop = v
		elif k == "punk":
			punk = v
		elif k == "metal":
			metal = v
		elif k == "electronic":
			electronic = v
		elif k == "folk":
			folk = v
		elif k == "indie":
			indie = v
		elif k == "blues":
			blues = v
		elif k == "hip hop":
			hip_hop = v
		elif k == "emo":
			emo = v
		elif k == "alternative":
			alternative = v
		else:
			altro = altro + v
	
	print user
	
	df3 = pd.DataFrame([ [user, ascolti_rock, pop, punk, metal, folk, electronic, indie, blues, hip_hop, emo, alternative, altro, flag] ], columns = list(colum))
	users = users.append(df3, ignore_index=False)

	with open('listening_user_freq.csv', 'a') as f:
		users.to_csv(f, header=False)
		
#	1 - regola che definisce l'abbandono:
# 	se il numero dei giorni in cui non ci sono ascolti è maggiore del 70% 
# 	del periodo totale di ascolto dell'utente;
def rule_70(days, listenings):
	
	n_days = len(days)
	
	listenings = [int(i) for i in listenings]
	
	percent = n_days * 0.7
	
	count = 0
	flag = False
	
	for item in listenings:
		if item == 0:
			count += 1
			
	#print "rule 70"		
	#print str(count) + " > " + str(percent) + " ?"
	if count >= percent:
		flag = True
	else:
		flag = False
	
	return flag

# 2 - regola che definisce l'abbandono:
# se il numero dei giorni consecutivi in cui non ci sono ascolti è maggiore 
# del 40% del periodo totale di ascolto dell'utente;
def rule_40(days, listenings):
	
	n_days = len(days)
	
	listenings = [int(i) for i in listenings]
	
	percent = n_days * 0.4
	
	count = 0
	flag = False
	
	for item in listenings:
		
		if item == 0:
			count += 1
			if count > percent:
				flag = True
				break
		else:
			count = 0
	
	#print "rule 30"		
	#print str(count) + " > " + str(percent) + " ?"
	return flag

# 3 - regola che definisce l'abbandono:
# dividendo a metà il periodo di ascolto e calcolando sulla prima parte 
# la moda (calcolo del valore più frequente), se il numero di picchi, della seconda parte, 
# minori del valore della moda è superiore al 90% degli ascolti fatti nella seconda parte;
def rule_90(days, listenings):
	
	listen1 = listenings[:len(listenings)/2]
	listen2 = listenings[len(listenings)/2:]
	
	if len(listen1) > 1:
		try:
			tresh = mode(listen1)
		except StatisticsError:
			tresh = sum(listen1)/len(listen1)
		
	else:
		tresh = listen1
	
	percent = len(listen2) * 0.9
	
	count = 0
	flag = False
	
	for item in listen2:
		if item < tresh:
			count += 1
			
	#print "rule 80"		
	#print str(count) + " >=" + str(percent) + " ?"
	if count >= percent:
		flag = True
		
	return flag

# 4 - regola che definisce l'abbandono:
# se il numero di ascolti sono maggiori rispetto all'ascolto successivo è maggiore del 60%.
def desc(listenings):
	
	flag = False
	count = 0
	percent = len(listenings) * 0.6
	
	for item in range(0, len(listenings) - 1):
		
		if listenings[item] > listenings[item + 1]:
			count += 1
	
	#print "rule_desc"
	#print str(count) + " > " + str(percent) + " ?"
	if count > percent:
		flag = True
	
	return flag


def insertionSort(alist, value):
	
   for index in range(1,len(alist)):

     currentvalue = alist[index]
     position = index
     
     currentvalue_value = value[index]
     position_value = index

     while position > 0 and alist[position-1] > currentvalue:
		 
         alist[position] = alist[position-1]
         
         value[position] = value[position-1]
        
         position = position-1

     alist[position] = currentvalue
     value[position] = currentvalue_value
   
   #plt_data = value
     
   return alist, value

if __name__ == "__main__":
	main()
