import pandas as pd

def main():
	"""
	#1 - subgenres to genres aggregation
	genres_substitution("listenings_def_fixed.csv")
	
	#loading dataset obtained
	df = pd.read_csv("listenings_subst_genres.csv", delimiter = ",", skipinitialspace = True)
	
	#2 - Saving datasets of the first five genres
	first_genres = ["rock", "pop", "electronic", "metal", "hip hop"]
	for item in first_genres:
		extract_dataset(df, item)	
	"""
def extract_dataset(data, genre):
	data = data[data["genre"] == genre]
	data.to_csv(genre + ".csv", index = False)
	
def genres_substitution(filename):
	df = pd.read_csv(filename, skipinitialspace = True, delimiter = ",")
	
	count = 0
	genres = ["rock", "pop", "metal", "electronic", "hip pop", "indie", "r&b", "punk", "folk"]
	for index, item in df.iterrows():
		for obj in genres:
			if obj in item["genre"]:
				df.set_value(index, "genre", obj)
				count += 1
				break
			print count
		
	df.to_csv("listenings_subst_genres.csv", index = False)
	
if __name__ == "__main__":
	main()
