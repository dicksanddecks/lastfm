import pandas as pd
import matplotlib.pyplot as plt

__author__ = "Dicks and Decks"

def main():
	
	#dataset loading - parameter: dataset filename
	df = load_csv("/home/fabrizio/Scrivania/Universita/Secondo semestre/Data mining II/project/dm2_2016_lastFM/listenings_def_fixed.csv")
	plt.style.use("dark_background")
	
	#variables distributions, best 10 results - parameter: column on which performs the count
	count_distr(df["genre"], "genre_before.png")
	#count_distr(df["artist"], "artist_before.png")


def load_csv(input_filename):
	
	return pd.read_csv(input_filename, skipinitialspace = True, delimiter=",", error_bad_lines = False)
	
def plot_distr(data, label):
	
	plt.ylabel("Numero di Ascolti")
	plt.xlabel("Genere")
	data.plot(kind='bar', alpha=0.7)
	plt.gcf().subplots_adjust(bottom=0.20)
	plt.savefig(label, bbox_inches = "tight")
	plt.show()

def count_distr(data, label):
	
	counts = data.value_counts()
	counts = counts[:10]
	print counts
	plot_distr(counts, label)

if __name__ == "__main__":
	main()
