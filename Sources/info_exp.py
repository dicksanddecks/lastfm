import pandas as pd

def main():
	
	info = pd.read_csv("informationOfUsers.csv", delimiter = ",", skipinitialspace = True)
	
	"""	
	print "Missing values"
	print info.isnull().sum()
	
	print "Value counts sesso"
	print len(info["sesso"].unique())
	print info["sesso"].value_counts()
	"""
	#aggrego male, female e null
	info["sesso"] = info["sesso"].replace("F", "f")
	info["sesso"] = info["sesso"].replace("M", "m")
	info["sesso"] = info["sesso"].replace("N", "n")
	info["sesso"] = info["sesso"].fillna('n')
	
	#elimino righe con valore null di sesso
	info = info[info["sesso"] != "n"]

	print info.isnull().sum()
	
	#cancello colonna paese
	info = info.drop("paese", 1)	
	info = info.drop("n_ascolti", 1)	
	
	print info.columns
	
	info.to_csv("info_def.csv", index = False)
	
if __name__ == "__main__":
	main()
