import pandas as pd
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.metrics import silhouette_score
from scipy.spatial.distance import pdist
from collections import Counter
from sklearn.neighbors import kneighbors_graph
import numpy as np

def main():
	
	#loading dataset
	df = pd.read_csv("../Datasets/final_seg.csv", delimiter = ",", skipinitialspace = True)
	df_final = pd.read_csv("../Datasets/final_seg.csv", delimiter = ",", skipinitialspace = True)
	#user_id column drop
	df = df.drop("user_id", 1)
	df = df.drop("data_reg", 1)
	df_final = df_final.drop("data_reg", 1)
	#normalization
	df = ((df - df.mean()) / (df.max() - df.min()))
	
	KMeans_clusters = k_means(df)
	DBScan_clusters = DBScan(df)
	
	df_final['KMeans'] = pd.Series(KMeans_clusters, index=df.index)
	df_final['DBScan'] = pd.Series(DBScan_clusters, index=df.index)
	#df_final.to_csv("../Datasets/segmentation_results_wo_reg.csv", index = False)	

def DBScan(df):
	"""
	# Matrice delle distanze per il calcolo di eps per 9 punti
	graph = kneighbors_graph(df,9,metric="euclidean",mode="distance")

	# Plot delle distanze in ordine crescente
	arr = []
	for m in np.asarray(graph.todense()):
		for dist in m:
			if dist > 0:
				arr.append(float(dist))
	arr.sort()
	plt.xlabel("Coppia di punti")
	plt.ylabel("Distanza")
	plt.title("K-Distance Graph")
	plt.plot(7500,0.35,marker="o",color="Red",label="eps")
	plt.plot(arr)
	plt.legend(fancybox=True, shadow=True,numpoints=1,handlelength=0)
	plt.show()
	"""
	dbscan = DBSCAN(eps=0.144963, min_samples=9, metric='euclidean').fit(df.values)
	
	percent_clusters(dbscan.labels_)
	
	return dbscan.labels_
	
def k_means(df):
	"""
	maxS = 0
	for clusters in range(2,100):
			kmeans = KMeans(init='k-means++', n_clusters=clusters, n_init=10, max_iter=100).fit(df.values)
			silhouette = silhouette_score(df.values,kmeans.labels_)
			clusters_final = clusters
			if silhouette > maxS:
				maxS = silhouette
				clusters_final = clusters
			print(maxS, clusters_final)
	print("Risultato: ",maxS, clusters_final)
	"""
	kmeans = KMeans(n_clusters=6, n_init=10, max_iter=100).fit(df.values)
		
	percent_clusters(kmeans.labels_)
	
	return kmeans.labels_
	
	
def percent_clusters(clusters):
	num_class = Counter(clusters)

	print("\nPercentuale elementi nel cluster")
	for key in num_class:
		print("Cluster ",key,": %.1f (%d elementi)" %(((num_class[key]*100)/len(clusters)),num_class[key]))

if __name__ == "__main__":
	main()
