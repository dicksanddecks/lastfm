import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

__author__ = "Dicks and Decks"

def main():

	"""
	#1 - loading listenings' csv file
	df = load_csv("listenings_20160403.csv")
	
	#2 - Saving correct csv
	save_csv(df, "listenings_20160403_fixed.csv")

	#3 - Checking missing values
	check_MV(df)

	#4 - removing rows with track and artist MV 
	df = remove_MV(df)
	save_csv(df)
	
	#5 - merging with genre dataset
	merge(df, "genre_20160403.csv")
	
	
	#loading merged file 	
	df = load_csv("listenings_genre_merged.csv")
	
	#basic dataset statistics print
	#print df.describe()
		
	#6 - deleting rows with date = 0
	df = load_csv("listenings_def.csv")
	df = deleteDateZero(df)
	
	#7 - deleting duplicate rows
	df = df.drop_duplicates()
	
	#save new csv 
	save_csv(df, "listenings_def_fixed.csv")
	"""
	
def load_csv(input_filename):
	
	return pd.read_csv(input_filename, skipinitialspace = True, delimiter=",", error_bad_lines = False)

def check_MV(data):

	print data.isnull().sum()

def remove_MV(data):
	
	data = data[pd.notnull(data['track'])]
	data = data[pd.notnull(data['artist'])]
	return data

def save_csv(data, filename):

	data.to_csv(filename, index = False)
	print "File successfully saved."

def merge(data, genre_filename):
	
	df_g = load_csv(genre_filename)
	dataset = pd.merge(df, df_g, left_on='artist', right_on='artist')
	save_csv(dataset, "listenings_genre_merged.csv")
	
def deleteDateZero(data):
	data = data[data["date"] != "0"]
	save_csv(data, "listenings_def.csv")
	return data
	
if __name__ == "__main__":
	main()
