import pandas as pd 
import matplotlib
import matplotlib.pyplot as plt
import os

def main():

	df = pd.read_csv("../Datasets/segmentation_results_wo_reg.csv", delimiter = ",", skipinitialspace = True)
	
	plt.style.use("dark_background")
	
	analysis(df, "KMeans")
	#analysis(df, "DBScan")

def analysis(df, alg_type):
	
	clusters = df[alg_type].unique()
	clusters.sort()
	for cluster in clusters:
		data = df[df[alg_type] == cluster]
		genres_distr(data, cluster, alg_type)
		sex_distr(data, cluster, alg_type)

def genres_distr(df, number, alg_type):
	
	genres_data = {'genre': ['ROCK', 'POP', 'ELECTRONIC', 'METAL', 'HIP HOP', 'PUNK', 'FOLK', 'INDIE', 'R&B', 'JAZZ', 'ALTRO'], 
        'n_listenings': [df["rock"].sum(), df["pop"].sum(), df["electronic"].sum(), df["metal"].sum(), df["hip hop"].sum(),
        df["punk"].sum(), df["folk"].sum(), df["indie"].sum(),df["r&b"].sum(), df["jazz"].sum(), df["altro"].sum()]}
	df = pd.DataFrame(genres_data, columns = ['genre', 'n_listenings'])

	pd.pivot_table(df, index = ["genre"]).plot(kind = "bar")
	plt.tight_layout()
	path = "/home/fabrizio/Scrivania/Segmentation_results/" + alg_type + "/Cluster" + str(number)
	if not os.path.exists(path):
		os.makedirs(path)
	filename = path + "/genres.png"
	plt.savefig(filename, bbox_inches = "tight")
	plt.close()

def sex_distr(df, number, alg_type):
	
	df["sesso"] = df["sesso"].replace(0, "UOMINI")
	df["sesso"] = df["sesso"].replace(1, "DONNE")
	sex = df["sesso"].value_counts()
	sex.plot(kind = "bar", width = 0.07)
	plt.tight_layout()
	path = "/home/fabrizio/Scrivania/Segmentation_results/" + alg_type + "/Cluster" + str(number)
	if not os.path.exists(path):
		os.makedirs(path)
	filename = path + "/sex.png"
	plt.savefig(filename, bbox_inches = "tight")
	plt.close()
	
	
if __name__ == "__main__":
	main()
	
