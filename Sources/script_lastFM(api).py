import urllib, json
import json
import matplotlib.pyplot as plt
from pprint import pprint
from collections import Counter
import pandas as pd
import collections
from pandas import Series
import pandas
import numpy as np

def main():
	
	key = "b25b959554ed76058ac220b7b2e0a026"
	
	#df = load_csv("network_20160403.csv")
	df = load_csv("new_network.csv")
	
	user = []
	temp = []
	count = 0
	colum = ["nome", "paese", "sesso", "n_ascolti", "data_reg"]
	
	#print df["user_id1"].value_counts()
	#print len(df["user_id1"].value_counts())
	
	for item in (df["user_id1"].unique()):
		
		url = "http://ws.audioscrobbler.com/2.0/?method=user.getinfo&user=" + item + "&api_key=" + key + "&format=json"
		response = urllib.urlopen(url)
		people = pd.DataFrame()
		
		if response.getcode() == 200:
			
			data = json.loads(response.read())
			
			
			if "name" in data["user"]:
				name = data["user"]["name"]
			else:
				name = ""
				
			if "country" in data["user"]:
				country = data["user"]["country"]
			else:
				country = ""
				
			if "gender" in data["user"]:
				gender = data["user"]["gender"]
			else:
				gender = ""
		
			if "playcount" in data["user"]:
				playcount = data["user"]["playcount"]
			else:
				playcount = ""
			
			if "registered" in data["user"]:
				if "unixtime" in data["user"]["registered"]:
					unixtime = data["user"]["registered"]["unixtime"]
			else:
				unixtime = ""
			
			temp.append(country)
			
			df = pd.DataFrame([ [name, country, gender, playcount, unixtime] ], columns = list(colum))
			#print df
		
			people = people.append(df, ignore_index=False)
		
		count += 1
		print count
			
		with open('people.csv', 'a') as f:
			people.to_csv(f, header=False)
			
	counter = collections.Counter(temp)
	
	s = Series(counter)
	
	s.plot(kind='bar', color='r')
	plt.ylabel("Number of people")
	plt.show()

if __name__ == "__main__":
	main()
